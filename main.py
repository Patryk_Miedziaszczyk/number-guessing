import random

print("Number Guessing Game")

chances = int(input("Enter chances: "))
random_number = random.randrange(1, 100)
for i in range(0,chances):
    number = int(input("Enter number: "))
    if random_number == number:
        print("Congratulations, You won!")

    elif random_number > number:
        print("Ups, Your number is too small")

    elif random_number < number:
        print("Ups, Your number is too high")
    if i == 9:
        print("Sorry, You've exhausted all your chances")